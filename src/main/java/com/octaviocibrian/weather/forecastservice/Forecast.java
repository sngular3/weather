package com.octaviocibrian.weather.forecastservice;

public class Forecast {

	private String LocalObservationDateTime;
	private String WeatherText;
	private Double TemperatureC;
	private Double TemperatureF;
	private String PrecipitationType;
	
	
	
	public Forecast(String localObservationDateTime, String weatherText, Double temperatureC, Double temperatureF,
                    String precipitationType) {
		super();
		LocalObservationDateTime = localObservationDateTime;
		WeatherText = weatherText;
		TemperatureC = temperatureC;
		TemperatureF = temperatureF;
		PrecipitationType = precipitationType;
	}
	public String getLocalObservationDateTime() {
		return LocalObservationDateTime;
	}
	public void setLocalObservationDateTime(String localObservationDateTime) {
		LocalObservationDateTime = localObservationDateTime;
	}
	public String getWeatherText() {
		return WeatherText;
	}
	public void setWeatherText(String weatherText) {
		WeatherText = weatherText;
	}
	public Double getTemperatureC() {
		return TemperatureC;
	}
	public void setTemperatureC(Double temperatureC) {
		TemperatureC = temperatureC;
	}
	public Double getTemperatureF() {
		return TemperatureF;
	}
	public void setTemperatureF(Double temperatureF) {
		TemperatureF = temperatureF;
	}
	public String getPrecipitationType() {
		return PrecipitationType;
	}
	public void setPrecipitationType(String precipitationType) {
		PrecipitationType = precipitationType;
	}
	
}
