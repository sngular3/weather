package com.octaviocibrian.weather.controller;

import com.octaviocibrian.weather.models.CurrentConditionsDTO;
import com.octaviocibrian.weather.repository.WeatherRepository;
import com.octaviocibrian.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class WeatherController {
    @Autowired
    private MongoTemplate mongoTemplate;

    Logger logger = LoggerFactory.getLogger(WeatherController.class);
    @Autowired
    private RestTemplateBuilder builder;

    @Autowired
    private WeatherService weatherService;

    @GetMapping("/weather/{searchWord}")
    public ResponseEntity<?> getForecast(@PathVariable String searchWord) throws Exception {
        if(searchWord == null) throw new Exception("No Param");
        try{
            CurrentConditionsDTO[] currentWeather =  weatherService.createWeather(searchWord);
            mongoTemplate.insert(currentWeather[0],"weather");
            return new ResponseEntity<>(currentWeather, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/history")
    public ResponseEntity<?> getTest() throws Exception {

        try{
            return new ResponseEntity<>(weatherService.testing(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
