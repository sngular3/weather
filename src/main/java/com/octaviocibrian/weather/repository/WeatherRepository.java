package com.octaviocibrian.weather.repository;

import com.octaviocibrian.weather.models.CurrentConditionsDTO;
import com.octaviocibrian.weather.models.db.WeatherDB;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WeatherRepository extends MongoRepository<WeatherDB, String> {


}
