package com.octaviocibrian.weather.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.octaviocibrian.weather.errorhandler.RestTemplateResponseErrorHandler;
import com.octaviocibrian.weather.models.CityDTO;
import com.octaviocibrian.weather.models.CurrentConditionsDTO;
import com.octaviocibrian.weather.repository.WeatherRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class WeatherService {
    @Autowired
    private WeatherRepository weatherRepository;

    @Autowired
    private MongoTemplate mongoTemplate;
    private CurrentConditionsDTO[] currentWeather;
    private String APIKey = "VXLMAMCUllfjc3VwPwu98pCtvMKKvd4o";
    public CurrentConditionsDTO[] createWeather(String searchWord) throws Exception {


        try{
            return searchByKey(searchWord);

        }catch(Exception e){
            try{
                return searchByCity(searchWord);

            }catch(Exception e2){
                try{
                    return searchByWord(searchWord);

                }catch(Exception e3){
                    throw new Exception(e2);
                }

            }
        }
    }

    public CurrentConditionsDTO[] searchByKey(String searchWord) throws Exception {
        String url = "http://dataservice.accuweather.com/currentconditions/v1/{locationKey}?apikey={queryParameter}";

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);

        Map<String, String> uriVariables = new HashMap<>();

        uriVariables.put("locationKey", searchWord);
        uriVariables.put("queryParameter", APIKey);

        try {
            ResponseEntity<CurrentConditionsDTO[]> responseCondition = template.exchange(url, HttpMethod.GET, requestEntity, CurrentConditionsDTO[].class, uriVariables);
            currentWeather = responseCondition.getBody();
            List<CurrentConditionsDTO> list = List.of(currentWeather);




        }catch(Exception e) {
            throw new Exception("Key not founded");
        }

        return currentWeather;
    }


    public CurrentConditionsDTO[] searchByWord(String searchWord) throws Exception {
        String url = "http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey={queryParameter}&q={city}";

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);

        Map<String, String> uriVariables = new HashMap<>();

        uriVariables.put("city", searchWord);
        uriVariables.put("queryParameter", APIKey);
        try{


            ResponseEntity<CityDTO[]> response = template.exchange(url, HttpMethod.GET, requestEntity, CityDTO[].class, uriVariables);
            CityDTO[] cities = response.getBody();

            searchByKey(cities[0].getKey());

            currentWeather[0].setCityName(cities[0].getLocalizedName());
            currentWeather[0].setCountryName(cities[0].getCountry().getLocalizedName());
            currentWeather[0].setStateName(cities[0].getAdministrativeArea().getLocalizedName());

        }catch(Exception e) {
            throw new Exception(e);
        }

        return currentWeather;
    }

    public CurrentConditionsDTO[] searchByCity(String searchWord) throws Exception {

        String url = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey={queryParameter}&q={city}";

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);

        Map<String, String> uriVariables = new HashMap<>();

        uriVariables.put("city", searchWord);
        uriVariables.put("queryParameter", APIKey);
        try{
            ResponseEntity<CityDTO[]> response = template.exchange(url, HttpMethod.GET, requestEntity, CityDTO[].class, uriVariables);
            CityDTO[] cities = response.getBody();

            searchByKey(cities[0].getKey());

            currentWeather[0].setCityName(cities[0].getLocalizedName());
            currentWeather[0].setCountryName(cities[0].getCountry().getLocalizedName());
            currentWeather[0].setStateName(cities[0].getAdministrativeArea().getLocalizedName());

        }catch(Exception e) {
            throw new Exception("No exist in the DB"+e);
        }

        return currentWeather;
    }

    public Object testing() {

        return weatherRepository.findAll();
    }


}
