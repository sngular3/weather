package com.octaviocibrian.weather.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemperatureDTO {

    @JsonProperty("Metric")
    private MetricDTO metric;

    @JsonProperty("Imperial")
    private ImperialDTO imperial;
}
