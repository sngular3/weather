package com.octaviocibrian.weather.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder


@JsonIgnoreProperties(ignoreUnknown = true)
public class CityDTO {
	
	@JsonProperty("Version")
	private int version;

	@JsonProperty("Key")
	private String key;
	
	@JsonProperty("Type")
	private String type;
	
	@JsonProperty("Rank")
	private int rank;
	
	@JsonProperty("LocalizedName")
	private String localizedName;
	
	@JsonProperty("Country")
	private CountryDTO country;
	
	@JsonProperty("AdministrativeArea")
	private AdministrativeAreaDTO administrativeArea;
}
