package com.octaviocibrian.weather.models.db;


import com.octaviocibrian.weather.models.TemperatureDTO;
import lombok.Data;
import lombok.Getter ;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Field;

@Data
@NoArgsConstructor
@org.springframework.data.mongodb.core.mapping.Document(collection = "weather")
public class WeatherDB {

    private String id;

    @Field("observationTime")
    private String observationTime;
    @Field("weatherDescription")
    private String weatherDescription;
    @Field("precipitationType")
    private String precipitationType;

    private TemperatureDTO temperature;
    @Field("cityName")
    private String cityName;
    @Field("stateName")
    private String stateName;
    @Field("countryName")
    private String countryName;

}
