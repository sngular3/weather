package com.octaviocibrian.weather.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryDTO {
	
	@JsonProperty("ID")
	private String Id;
	
	@JsonProperty("LocalizedName")
	private String localizedName;
}
