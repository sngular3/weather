package com.octaviocibrian.weather.models;


import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentConditionsDTO {

    //@JsonProperty("LocalObservationDateTime")
    @JsonAlias({ "LocalObservationDateTime" })
    private String observationTime;

    @JsonAlias("WeatherText")
    private String weatherDescription;

    @JsonAlias("PrecipitationType")
    private String precipitationType;

    @JsonAlias("Temperature")
    private TemperatureDTO temperature;

    private String cityName;
    private String stateName;
    private String countryName;

}
