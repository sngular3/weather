package com.octaviocibrian.weather.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministrativeAreaDTO {
	
	@JsonProperty("ID")
	private String id;
	
	@JsonProperty("LocalizedName")
	private String localizedName;
}
