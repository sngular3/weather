import com.octaviocibrian.weather.controller.WeatherController
import com.octaviocibrian.weather.models.CurrentConditionsDTO
import com.octaviocibrian.weather.service.WeatherService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import spock.lang.Specification



//@WebMvcTest(controllers = [WeatherController])

//@SpringBootTest
//@SpringBootTest(classes = WeatherController.class)
class ControllerTest extends  Specification{

    @Autowired
    protected MockMvc mvc


    //def "should be a simple assertion"(){

      //  expect:
            //mvc.perform(get("/weather/1310").andExpect(status().isOk()))
    //}

    def "The API Key is correct"(){


        setup:
            def weatherService = new WeatherService()
        when:
            def result = weatherService.getAPIKey()

        then:
            result == "VXLMAMCUllfjc3VwPwu98pCtvMKKvd4o"



    }

    def "The locationKey is working"(){


        setup:
        def weatherService = new WeatherService()
        when:
        def result = weatherService.createWeather("1310")[0].getObservationTime()

        then:
        result != null



    }

    def "The locationKey Searching with the City is working"(){


        setup:
        def weatherService = new WeatherService()
        when:
        def result = weatherService.createWeather("Pittsburgh PA")[0].getCityName()

        then:
        result == "Pittsburgh"



    }

    def "The locationKey With any Word Search is working"(){


        setup:
        def weatherService = new WeatherService()
        when:
        def result = weatherService.createWeather("Pittsb")[0].getCityName()

        then:
        result == "Pittsburgh"

    }
}
