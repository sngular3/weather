# weather

Project that fetch the  [API AccuWeather](https://developer.accuweather.com/user/login?destination=user/me/apps&autologout_timeout=1)

`http://localhost:8080/weather/wordSearch`

Where `wordSearch` could be the **locationKey**, a **city** e.g(Pittsburgh PA) or a **word to search** e.g(Pittsb) 

Endpoint `http://localhost:8080/history` to see the History of the previous searches.
